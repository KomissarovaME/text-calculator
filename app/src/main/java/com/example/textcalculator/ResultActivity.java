package com.example.textcalculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends Activity {

    TextView firstOperand;
    TextView secondOperand;
    TextView results;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();

        firstOperand = findViewById(R.id.firstOperand);
        secondOperand = findViewById(R.id.secondOperand);
        results = findViewById(R.id.result);

        int first = intent.getIntExtra("first", 0);
        int second = intent.getIntExtra("second", 0);
        int result = intent.getIntExtra("result", 0);
        //String act=intent.getStringExtra("act");
        firstOperand.setText(String.valueOf(first));
        secondOperand.setText(String.valueOf(second));
        results.setText(String.valueOf(result));
    }
}

