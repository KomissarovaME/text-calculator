
package com.example.textcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private int firstOperand;
    private int secondOperand;
    private EditText getFirstValue;
    private EditText getSecondValue;
    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getFirstValue = findViewById(R.id.firstOperand);
        getSecondValue = findViewById(R.id.secondOperand);
    }

    public void onClick(View view) {
        //Button button=(Button) view;
        //String lastOperation = button.getText().toString();

        firstOperand = Integer.parseInt(getFirstValue.getText().toString());
        secondOperand = Integer.parseInt(getSecondValue.getText().toString());
        result=firstOperand+secondOperand;
        /*int result=0;
        switch(lastOperation){
            case "/":
                if(secondOperand==0){
                    putKeysIntoIntent(result,lastOperation);
                }
                else{
                    result=(int)firstOperand/secondOperand;
                    putKeysIntoIntent(result, lastOperation);
                }
                break;
            case "*":
                result=(int)firstOperand*secondOperand;
                putKeysIntoIntent(result, lastOperation);
                break;
            case "+":
                result=(int)firstOperand+secondOperand;
                putKeysIntoIntent(result, lastOperation);
                break;
            case "-":
                result=(int)firstOperand-secondOperand;
                putKeysIntoIntent(result, lastOperation);
                break;
        }*/
        putKeysIntoIntent();
    }

    private void putKeysIntoIntent() {
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("first", firstOperand);
        intent.putExtra("second", secondOperand);
        intent.putExtra("result", result);
        startActivity(intent);
    }

}